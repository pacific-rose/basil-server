const express = require('express')
const app = express()
const port = 3000
const path = require('path')

// Serve the app associated domain file as a static file
app.use(express.static('public'))

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/oauth', (req, res) => {
    res.send('OAuth redirect URI')
})

app.get('/link-account', (req, res) => {
  res.sendFile(path.join(__dirname + '/public/plaid-link.html'))
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})