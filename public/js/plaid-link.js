const handler = Plaid.create({
    onSuccess: (public_token, metadata) => {
        console.log(`Success, public token: ${public_token}, metadata: ${metadata}`)
    },
    onExit: (err, metadata) => {
        console.log(`Exit error: ${err}, metadata: ${metadata}`)
    },
    onEvent: (eventName, metadata) => {
        console.log(`Event: ${eventName}, metadata: ${metadata}`)
    },
    token: 'link-sandbox-e8c7a87b-8b6a-4e2d-b389-b4504dea76f1',
    // //required for OAuth; if not using OAuth, set to null or omit:
    // receivedRedirectUri: 'https://finance.pacificrose.dev/oauth'
});

handler.open()